var multer = require('multer')
var express = require('express')
var router = express.Router();


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './upload/')
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
})

let upload = multer({ storage: storage });

router.post('/profile', upload.single('product_image'), function (req, res) {
    console.log(req.file.path)
    
    const { body } = req;
    if (body === {} || body === '')
        return res.status(505).json({ statusCode: 505, message: 'updateAdd object can not be empty', data: '' });
    body['product_image'] = req.file.originalname;
})

module.exports = router