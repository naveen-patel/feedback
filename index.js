const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const db = require('./queries');
const multer = require('multer');
const port = 3000;
var cron = require('node-cron');
var jwt = require('jsonwebtoken');

// for cron job automatically function calling
// cron.schedule('*/2 * * * *', () => {
//   db.cronJob();
// });


const storage = multer.diskStorage({
  destination: function (request, file, cb) {
    cb(null, './upload/')
  },
  filename: function (request, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
})
let upload = multer({ storage: storage });

var publicDir = require('path').join(__dirname, '/upload');
app.use(express.static(publicDir));


app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());
app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

// for  JSON web token
let verifyToken = function (req, res, next) {
  const bearerHeader = req.headers['authorization'];
  const token = bearerHeader;
  req.token = token;
  jwt.verify(token, 'privateKey', function (err, authData) {
    if (!err) {
      req.body.id = authData.id
      next();
    }
    else {
      return res.sendStatus(403);
    }
  });

}
app.post('/users', upload.single('product_image'), db.createUser)
app.post('/login', db.userLogin)
app.get('/dashboard', verifyToken, db.dashboardData)
app.get('/users', db.cronJob)
app.get('/receiver', verifyToken, db.feedbackForm)
app.post('/feedbacksave', verifyToken, db.senderFeedbackSave)



app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})