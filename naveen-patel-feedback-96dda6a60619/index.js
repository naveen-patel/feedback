const express = require('express')
const bodyParser = require('body-parser')
const app = express();
const cors = require('cors');
const db = require('./queries')
var multer = require('multer')
const port = 3000
const profile=require('./profile')
var cron = require('node-cron');

cron.schedule('*/1 * * * *', () => {
  db.getUsers();
});


const storage = multer.diskStorage({
  destination: function (request, file, cb) {
      cb(null, './upload/')
  },
  filename: function (request, file, cb) {
      cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
})
let upload = multer({ storage: storage });


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})


// app.use((req,res,next) => {
//     console.log('Path =>', req.path )
//     console.log('Method => ', req.method)
//     next()
// })
app.post('/profile',profile)
app.get('/users', db.getUsers)
app.get('/users/:id', db.getUserById)
app.post('/users',upload.single('product_image'), db.createUser)
// app.post('/feedback', db.createFeedback)
app.get('/feedback/:id', db.getFeedbackById)
app.post('/schedulefeedback', db.createSchedule)
app.post('/login', db.getLogin)
app.get('/receiver/:id', db.getReceiverById)
app.post('/feedbacksave/:id', db.getSenderFeedbackSave)


app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})