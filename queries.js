const express = require('express')
const router = express.Router();
const sendmail = require('sendmail')()
const bcrypt = require('bcrypt');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'naveen',
  host: 'localhost',
  database: 'feedbackdb',
  password: 'patel123',
  port: 5432,
})
// checking server connect or not 
pool.connect((error, results) => {
  if (error) {
    response.status(400).json({ error, code: 400, message: 'server not connected' })
  }
})

// Api for cron job (scheduler)
const cronJob = (request, response) => {
  pool.query('truncate schedule')
  pool.query('SELECT dev_id,name,email_id,profile_photo FROM registration', (error, results) => {
    if (error) {
      return response.status(400).json({ error, code: 400, message: 'Database query error' })
    }
    var userDataCron = results.rows
    var randomSender = [];

    while (randomSender.length < 10) {
      var newSenderData = Math.floor(Math.random() * userDataCron.length);
      if (randomSender.indexOf(newSenderData) === -1) randomSender.push(newSenderData)
    }
    for (var i = 0; i < randomSender.length; i++) {
      var randomUsers = [];
      var selectedSender = randomSender[i];
      while (randomUsers.length < 3) {
        var newRec = Math.floor(Math.random() * userDataCron.length)
        if (randomUsers.indexOf(newRec) === -1 && newRec !== selectedSender) {
          randomUsers.push(newRec);
        }
      }
      var no = userDataCron[selectedSender].dev_id
      var emailSendName = userDataCron[selectedSender].name
      var emailSend = userDataCron[selectedSender].email_id
      for (var k = 0; k < randomUsers.length; k++) {

        var senderId = userDataCron[randomUsers[k]].dev_id
        var senderName = userDataCron[randomUsers[k]].name
        var senderProfileImage = userDataCron[randomUsers[k]].profile_photo
        pool.query('INSERT INTO schedule values ($1,$2,$3,$4)', [no, senderId, senderName, senderProfileImage], (error, results) => {
          if (error) {
            return response.status(400).json({ error, code: 400, message: 'Database query error' })
          }

        })
      }
      var output = `
    <h3>Hi ${emailSendName} please give feedback for :</h3>
    <ul>
    <li>Name:${userDataCron[randomUsers[0]].name}</li>
    <li>Name:${userDataCron[randomUsers[1]].name}</li>
    <li>Name:${userDataCron[randomUsers[2]].name}</li>
    <li><a href="http://10.0.100.221:4030">Visit here</a></li>
    </ul>`
      sendmail({
        from: 'naveen.patel@neosofttech.com',
        to: 'naveen',
        subject: 'Feedback details',
        html: output,
      }, function (err, reply) {
        console.log(err && err.stack);

      })
    }
  })
  return response.status(200).json({ code: 200, message: 'new receiver list updated' })
}


// for user registration
const createUser = (request, response) => { 
  try{
  const { name, email } = request.body
  const { file } = request;
  var ccEmail = 'naveenpatel0202@gmail.com'
  if (file === undefined) {
    return response.status(404).json({ error: { message: "image is required" } });
  }
  // for validation
  const schema = Joi.object().keys({
    name: Joi.string().regex(/^[^-\s][a-zA-Z\s-]+$/).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required()
  })
  Joi.validate(request.body, schema, function (err, value) {
    if (err) {
      return response.status(400).json({ err, code: 400, message: 'joi validation error' })
    }
    else {
      var passwordarr = [
        "Aardvark",
        "Albatross",
        "Alligator",
        "Alpaca",
        "Ant",
        "Anteater",
        "Antelope",
        "Ape",
        "Armadillo",
        "Donkey",
        "Baboon",
        "Badger",
        "Barracuda",
        "Bat",
        "Bear",
        "Beaver",
        "Bee",
        "Bison",
        "Boar",
        "Buffalo",
        "Butterfly",
        "Camel",
        "Capybara",
        "Caribou",
        "Cassowary",
        "Cat",
        "Caterpillitems",
        "Cattle",
        "Chamois",
        "Cheetah",
        "Chicken",
        "Chimpanzee",
        "Chinchilla",
        "Chough",
        "Clam",
        "Cobra",
        "Cockroach",
        "Cod",
        "Cormorant",
        "Coyote",
        "Crab",
        "Crane",
        "Crocodile",
        "Crow",
        "Curlew",
        "Deer",
        "Dinosaur",
        "Dog",
        "Dogfish",
        "Dolphin",
        "Dotterel",
        "Dove",
        "Dragonfly",
        "Duck",
        "Dugong",
        "Dunlin",
        "Eagle",
        "Echidna",
        "Eel",
        "Eland",
        "Elephant",
        "Elk",
        "Emu",
        "Falcon",
        "Ferret",
        "Finch",
        "Fish",
        "Flamingo",
        "Fly",
        "Fox",
        "Frog",
        "Gaur",
        "Gazelle",
        "Gerbil",
        "Giraffe",
        "Gnat",
        "Gnu",
        "Goat",
        "Goldfinch",
        "Goldfish",
        "Goose",
        "Gorilla",
        "Goshawk",
        "Grasshopper",
        "Grouse",
        "Guanaco",
        "Gull",
        "Hamster",
        "Hare",
        "Hawk",
        "Hedgehog",
        "Heron",
        "Herring",
        "Hippopotamus",
        "Hornet",
        "Horse",
        "Human",
        "Hummingbird",
        "Hyena",
        "Ibex",
        "Ibis",
        "Jackal",
        "Jaguar",
        "Jay",
        "Jellyfish",
        "Kangaroo",
        "Kingfisher",
        "Koala",
        "Kookabura",
        "Kouprey",
        "Kudu",
        "Lapwing",
        "Lark",
        "Lemur",
        "Leopard",
        "Lion",
        "Llama",
        "Lobster",
        "Locust",
        "Loris",
        "Louse",
        "Lyrebird",
        "Magpie",
        "Mallard",
        "Manatee",
        "Mandrill",
        "Mantis",
        "Marten",
        "Meerkat",
        "Mink",
        "Mole",
        "Mongoose",
        "Monkey",
        "Moose",
        "Mosquito",
        "Mouse",
        "Mule",
        "Narwhal",
        "Newt",
        "Nightingale",
        "Octopus",
        "Okapi",
        "Opossum",
        "Oryx",
        "Ostrich",
        "Otter",
        "Owl",
        "Oyster",
        "Panther",
        "Parrot",
        "Partridge",
        "Peafowl",
        "Pelican",
        "Penguin",
        "Pheasant",
        "Pig",
        "Pigeon",
        "Pony",
        "Porcupine",
        "Porpoise",
        "Quail",
        "Quelea",
        "Quetzal",
        "Rabbit",
        "Raccoon",
        "Rail",
        "Ram",
        "Rat",
        "Raven",
        "Red deer",
        "Red panda",
        "Reindeer",
        "Rhinoceros",
        "Rook",
        "Salamander",
        "Salmon",
        "Sand Dollar",
        "Sandpiper",
        "Sardine",
        "Scorpion",
        "Seahorse",
        "Seal",
        "Shark",
        "Sheep",
        "Shrew",
        "Skunk",
        "Snail",
        "Snake",
        "Sparrow",
        "Spider",
        "Spoonbill",
        "Squid",
        "Squirrel",
        "Starling",
        "Stingray",
        "Stinkbug",
        "Stork",
        "Swallow",
        "Swan",
        "Tapir",
        "Tarsier",
        "Termite",
        "Tiger",
        "Toad",
        "Trout",
        "Turkey",
        "Turtle",
        "Viper",
        "Vulture",
        "Wallaby",
        "Walrus",
        "Wasp",
        "Weasel",
        "Whale",
        "Wildcat",
        "Wolf",
        "Wolverine",
        "Wombat",
        "Woodcock",
        "Woodpecker",
        "Worm",
        "Wren",
        "Yak",
        "Zebra"
      ]
      var password = passwordarr[Math.floor(Math.random() * passwordarr.length)] + 123;
      const saltRounds = 10
      const myPlaintextPassword = password
      const salt = bcrypt.genSaltSync(saltRounds)
      const passwordHash = bcrypt.hashSync(myPlaintextPassword, salt)
      var image = request.file.filename
      const { body } = request;
      if (body === {} || body === '')
        return response.status(505).json({ statusCode: 505, message: 'updateAdd object can not be empty', data: '' });
      body['product_image'] = request.file.originalname;

      pool.query("INSERT INTO registration (name, email_id,password,profile_photo) values ($1,$2,$3,$4)", [name, email, passwordHash, image],
        (error, results) => {
          if (error) {
            return response.status(400).json({ code: 400, error: error.detail, message: 'database error' })
          }

          var output = `
      <h3>Hi ${name} your details are mentioned below:</h3>
      <ul>  
        <li>Name:${name}</li>
        <li>Email:${email}</li> 
        <li>Password: ${password}</li>
        <li><a href="http://10.0.100.221:4030">Visit here</a></li>
      </ul>`
          sendmail({
            from: 'naveen.patel@neosofttech.com',
            to: email,
            cc: ccEmail,
            subject: 'User login details',
            html: output,
          }, function (err, reply) {
            console.log(err && err.stack);

          })

          return response.status(200).send({ code: 200, message: 'User registered ', name: name });

        })

    }
  });
}
catch(error){
  if(error instanceof ReferenceError){
    console.log("raference error",error)
  }
}
}

// for user login
const userLogin = (request, response) => {
  const { email, password } = request.body
  // for validation
  const schema = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().required()
  })
  Joi.validate(request.body, schema, function (err, value) {
    if (err) {
      return response.status(400).json({ error: err.details, message: 'validation error1' })
    }
    else {

      pool.query('SELECT * FROM registration where email_id=$1', [email], (error, results) => {
        if (error) {
          response.status(400).json({ error: error, message: 'database error' })
          return;
        }
        if (results.rows.length === 0) {
          response.status(404).json({ code: 404, message: 'mail not found' });
        }
        else {
          if (bcrypt.compareSync(password, results.rows[0].password)) {


            jwt.sign({ email: email, id: results.rows[0].dev_id }, 'privateKey', function (err, token) {
              if (err) {
                return response.status(403).json({ code: 403, message: 'Token generator problems' })
              }
              else {
                response.status(200).json({ code: 200, message: 'successfully logged in', token: token, name: results.rows[0].name })
              }
            })

          }
          else {
            response.status(403).json({ code: 403, message: 'password not match' })
          }

        }
      })
    }
  })
}

// for dashboard data
const dashboardData = (request, response) => {
  const { id } = request.body
  pool.query('SELECT * FROM registration where dev_id=$1', [id], (error, results) => {
    if (error) {
      response.status(400).json({ code: 400, error: error, message: 'database error' })
      return;
    }
    else {
      pool.query('SELECT * FROM feedback where dev_id=$1', [results.rows[0].dev_id], (error, results1) => {
        if (error) {
          response.status(400).json({ code: 400, error: error, message: 'database error' })
          return;
        }
        else {
          var fb = [];
          for (var t = 0; t < results1.rows.length; t++) {
            fb.push(results1.rows[t])
          }
          setTimeout(() => response.status(200).json({ code: 200, message: 'token verified ', id: results.rows[0].dev_id, name: results.rows[0].name, image: results.rows[0].profile_photo, feedback: fb }), 2000);
          //  response.status(200).json({ code: 200, message: 'token verified ', id: results.rows[0].dev_id, name: results.rows[0].name, image: results.rows[0].profile_photo, feedback: fb })
        }
      })
    }
  })
}

//random receiver name for add feedback on (add button)
const feedbackForm = (request, response) => {
  const { id } = request.body
  pool.query('SELECT * FROM schedule WHERE dev_id1 = $1 and flags=$2', [id, false], (error, results) => {
    if (error) {
      response.status(400).json({ code: 400, error: error, message: 'database error' })
      return;
    }
    response.status(200).json(results.rows)
  })
}

//save feedback in db hit on save button
const senderFeedbackSave = (request, response) => {
  const { id, id2, feedback } = request.body
 
  // for validation
  const schema = Joi.object().keys({
    feedback: Joi.string().required(),
    id2:Joi.number().required(),
    id:Joi.number().required()
  })
  Joi.validate(request.body, schema, function (err, value) {
    if (err) {
      return response.status(400).json({ code: 400, error: err.details, message: 'validation error' })
    }
    else {

      pool.query('SELECT * FROM schedule WHERE dev_id1 = $1 and dev_id2=$2', [id, id2], (error, results1) => {
        if (error) {
          response.status(400).json({ code: 400, error: error, message: 'database error' })
          return;
        }
        if (results1.rows[0].flags) {
          response.status(403).json({ code: 403, message: 'Your feedback is already inserted' });
        }
        else {
          pool.query('INSERT INTO feedback (dev_id,feedback) values ($1,$2)', [id2, feedback], (error, results) => {
            if (error) {
              return response.status(400).json({ code: 400, error: error, message: 'database error' })

            }
            pool.query('UPDATE schedule SET flags=true WHERE dev_id1=$1 and dev_id2=$2;', [id, id2])
            return response.status(200).json({ code: 200, message: 'Your feedback is inserted' });
          })
        }
      })
    }
  })
}

module.exports = {
  cronJob: cronJob,
  createUser: createUser,
  userLogin: userLogin,
  router: router,
  feedbackForm: feedbackForm,
  senderFeedbackSave: senderFeedbackSave,
  dashboardData: dashboardData
}